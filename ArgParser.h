/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ArgParser.h
 * Author: sdn 
 *
 * Created on August 9, 2019, 9:52 AM
 */

#include <type_traits>
#include "Args.h"
#include "common.h"
using namespace std;

#ifndef ARGPARSER_H
#define ARGPARSER_H

class ArgParser {
public:
    ArgParser(const char** argv, int argc);
    ArgParser(const char** argv, int argc, std::ostream *logger);
    virtual ~ArgParser();
    ArgMap* parse();

    template<class TT>
    TT* parse_typed() {
        if (!std::is_base_of<Args, TT>::value)
        {
            *_logger << "Error: not a child of Args: " << typeid(TT).name() << endl;
            return nullptr;
        }
        TT* args_child_instance = new TT();
        // Args* typed_args = args_child_instance;
        args_child_instance->set_arg_map(this->parse());
        return args_child_instance;
    }

private:
    const char** _argv;
    const int _argc;
    std::ostream* _logger;

    int
    has_args(std::string* switch_name,
            ArgList* switch_args);
    int
    add_entry(ArgMap* arg_map,
            string* key, ArgList* values);
};

#endif /* ARGPARSER_H */

