/* 
 * File:   CharactersArgs.h
 * Author: bogdan
 *
 * Created on August 11, 2019, 9:25 PM
 */

#ifndef CHARACTERSARGS_H
#define	CHARACTERSARGS_H

#include "Args.h"

class CharactersArgs : public Args{
public:
    CharactersArgs();
    CharactersArgs(const CharactersArgs& orig);
    virtual ~CharactersArgs();
private:

};

#endif	/* CHARACTERSARGS_H */

