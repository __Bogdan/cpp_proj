/* 
 * File:   common.h
 * Author: bogdan
 *
 * Created on August 11, 2019, 9:37 PM
 */

#ifndef COMMON_H
#define	COMMON_H

#include <map>
#include <vector>
#include <fstream>

using namespace std;
typedef vector<string*> ArgList;
typedef std::map<std::string, ArgList*> ArgMap;


#endif	/* COMMON_H */

