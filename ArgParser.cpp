
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ArgParser.cpp
 * Author: sdn
 * 
 * Created on August 9, 2019, 9:52 AM
 */

#include "ArgParser.h"
#include "Args.h"
#include <fstream>
#include <vector>
#include <iterator>
#include <string>

using namespace std;

ArgParser::ArgParser (const char** argv, const int argc)
: _argv (argv), _argc (argc) { }

ArgParser::ArgParser (const char** argv, int argc, ostream *logger)
: _argv (argv), _argc (argc), _logger (logger) { }

ArgParser::~ArgParser () { }

ArgMap*
ArgParser::parse ()
{
  char** argv = const_cast<char**> (_argv);
  vector<string> args (argv, argv + _argc);

  auto result = new ArgMap();

  // Allocate memory for current switch
  auto curr_arg_list = new ArgList();
  string* curr_switch = new string();

  for (auto& arg : args)
  {
    *_logger << "Argument: " << arg << endl;
    if (arg[0] == '-')
    {
      if (this->has_args (curr_switch, curr_arg_list))
      {
        if (!this->add_entry (result, curr_switch, curr_arg_list))
        {
          return result;
        }
      }

      // Reallocate memory for current switch
      curr_switch = new string(arg);
      auto new_arg_list = new ArgList();
      curr_arg_list = new_arg_list;
    }
    else
    {
      curr_arg_list->push_back (new string(arg));

      if (&arg == &args[args.size () - 1])
      {
        this->add_entry(result, curr_switch, curr_arg_list);
      }
    }
  }
  
  *_logger << "Result map handles:" << endl;
  for (auto const& pair : *result)
  {
    *_logger << (long)pair.second << endl;
  }
  auto r1 = result;
  return r1;
}


int
ArgParser::has_args (std::string* switch_name,
                     ArgList* switch_args)
{
  *_logger << __FUNCTION__ << endl;
  *_logger << "String: " << *switch_name << endl;
  *_logger << "Vector size: " << switch_args->size() << endl;
  return (switch_name->length () > 0) && (switch_args->size() > 0);
}

int
ArgParser::add_entry (ArgMap* arg_map,
                      string* key, ArgList* values)
{
  const auto pair = arg_map->insert ({*key, values});
  const int has_succeeded = pair.second;
  if (!has_succeeded)
  {
    *_logger << "Parsing error for: " << *key << endl;
    for (auto arg : *values)
    {
      *_logger << *arg << ", ";
    }
  }
  return has_succeeded;
}