/* 
 * File:   Args.h
 * Author: bogdan
 *
 * Created on August 11, 2019, 9:25 PM
 */

#ifndef ARGS_H
#define	ARGS_H

#include "common.h"

class Args {
public:
    Args();
    Args(const Args& orig);
    virtual ~Args();
    void set_arg_map(ArgMap* map);
private:
    ArgMap* _arg_map;
};

#endif	/* ARGS_H */

