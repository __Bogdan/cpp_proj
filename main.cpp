/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: sdn 
 *
 * Created on August 9, 2019, 9:51 AM
 */

#include <cstdlib>
#include <iostream>
#include "ArgParser.h"
#include "CharactersArgs.h"

using namespace std;
/*
 * 
 */
int main(int argc, char** argv)
{
  std::ostream *logger = &std::cout;
  ArgParser parser(const_cast<const char**>(argv), argc, logger);
  auto arg_map = parser.parse();
  *logger << "Arg map:" << endl;
  for (auto const& pair : *arg_map)
  {
    *logger << pair.first << ": " << pair.second->size() << ", " << (long)pair.second << endl;
  }
  
  // TODO print handles here
  auto char_args = parser.parse_typed<CharactersArgs>();
  return 0;
}
