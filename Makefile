SOURCES=$(shell ls *.cpp)
PROG=$(notdir $(shell pwd))


CXX = g++	# ensure GNU
CC = g++	 # ensure GNU -- many use gcc here, but I get linker errors using gcc
ifdef windir
		CPPFLAGS += $(WIN32_CPPFLAGS)
endif

OBJS = $(SOURCES:.cpp=.o)

# default target, with implicit build rules
$(PROG): $(OBJS)
		$(LINK.o) $^ $(LOADLIBES) $(LDLIBS) -o $@

$(OBJS): $(SOURCES) 
		$(CXX) $(CPPFLAGS) -c -o $@ $<

.PHONY: clean		# .PHONY = always build target (don’t look for it in the filesystem)
clean:
	rm -rf $(PROG)
	rm -rf *.o

